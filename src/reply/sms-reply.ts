import {
  InteractionInterface,
  CommunicationTypeEnum,
  PhoneNumberChannelEnum,
  ReplyDataInterface,
  SmsSourceInterface,
  SourceDestinationTypeEnum,
} from "@timerocket/assistant-data-model";
import { SmsSource } from "./sms-source";
import { v4 as uuidV4 } from "uuid";

export class SmsReply implements ReplyDataInterface<SmsSourceInterface> {
  public interaction: InteractionInterface;
  public readonly source: SmsSourceInterface;
  public readonly text: string;
  messageId: string;
  type: CommunicationTypeEnum;

  constructor(
    interaction: InteractionInterface,
    sentFrom: string,
    sentTo: string,
    channel: PhoneNumberChannelEnum,
    message: string
  ) {
    this.source = new SmsSource(sentTo, channel, {
      phoneNumber: sentFrom,
      type: SourceDestinationTypeEnum.PHONE_SMS,
    });
    this.text = message;
    this.messageId = uuidV4();
    this.type = CommunicationTypeEnum.TEXT;
  }
}
