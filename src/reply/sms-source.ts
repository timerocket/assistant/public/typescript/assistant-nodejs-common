import {
  PhoneNumberChannelEnum,
  PhoneSmsDevice,
  SmsSourceInterface,
} from "@timerocket/assistant-data-model";

export class SmsSource implements SmsSourceInterface {
  constructor(
    public readonly sentToPhoneNumber: string,
    public readonly channel: PhoneNumberChannelEnum,
    public readonly device: PhoneSmsDevice
  ) {}
}
