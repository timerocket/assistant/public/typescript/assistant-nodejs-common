import { ArgumentMetadata, PipeTransform } from "@nestjs/common";
export declare class PhoneNumberChannelEnumPipe implements PipeTransform {
    transform(value: any, metadata: ArgumentMetadata): any;
}
