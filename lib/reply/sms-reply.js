"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmsReply = void 0;
const assistant_data_model_1 = require("@timerocket/assistant-data-model");
const sms_source_1 = require("./sms-source");
const uuid_1 = require("uuid");
class SmsReply {
    constructor(interaction, sentFrom, sentTo, channel, message) {
        this.source = new sms_source_1.SmsSource(sentTo, channel, {
            phoneNumber: sentFrom,
            type: assistant_data_model_1.SourceDestinationTypeEnum.PHONE_SMS,
        });
        this.text = message;
        this.messageId = uuid_1.v4();
        this.type = assistant_data_model_1.CommunicationTypeEnum.TEXT;
    }
}
exports.SmsReply = SmsReply;
