import { InteractionInterface, CommunicationTypeEnum, PhoneNumberChannelEnum, ReplyDataInterface, SmsSourceInterface } from "@timerocket/assistant-data-model";
export declare class SmsReply implements ReplyDataInterface<SmsSourceInterface> {
    interaction: InteractionInterface;
    readonly source: SmsSourceInterface;
    readonly text: string;
    messageId: string;
    type: CommunicationTypeEnum;
    constructor(interaction: InteractionInterface, sentFrom: string, sentTo: string, channel: PhoneNumberChannelEnum, message: string);
}
