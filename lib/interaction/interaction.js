"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Interaction = void 0;
class Interaction {
    constructor(id, categoryId) {
        this.id = id;
        this.categoryId = categoryId;
    }
}
exports.Interaction = Interaction;
